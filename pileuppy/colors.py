try:
    import colored
except ImportError:
    pass

from pileuppy._version import __version__

__author__ = 'Timofey Prodanov, Vikas Bansal'
__credits__ = __author__.split(', ')
__license__ = 'MIT'
__maintainer__ = __credits__[0]


class Colors:
    def __init__(self, scheme, use_logo):
        self.use_color = True
        self.use_logo = use_logo

        if scheme == 'black':
            self.column='grey_11'
            # [deletion, insertion, nt]
            self.error = ['red_1', 'light_goldenrod_2b', 'red_1']
            # [deletion, insertion, A, C, G, T]
            self.logo = ['red_1', 'hot_pink_1a', 'green_yellow', 'light_slate_blue', 'dark_orange', 'deep_pink_2']
            # [quality color, nt color]
            self.low_bq = ['red_1', 'grey_50']
            self.low_mapq = 'red_1'
            self.supplementary = 'dark_blue'
            self.out_of_region = 'grey_58'
            self.clipping = 'light_cyan'

        elif scheme == 'white':
            self.column = 'grey_93'
            self.error = ['red_1', 'purple_3', 'red_1']
            self.logo = ['red_1', 'dark_red_2', 'green', 'blue_1', 'dark_orange', 'red_1']
            self.low_bq = ['red_1', 'grey_50']
            self.low_mapq = 'red_1'
            self.supplementary = 'cyan_1'
            self.out_of_region = 'grey_46'
            self.clipping = 'magenta'

        elif scheme == 'ansi':
            self.column = None
            self.error = ['light_red', 'yellow', 'light_red']
            self.logo = ['light_red', 'magenta', 'green', 'light_blue', 'yellow', 'light_red']
            self.low_bq = ['light_red', 'dark_gray']
            self.low_mapq = 'light_red'
            self.supplementary = 'light_blue'
            self.out_of_region = 'dark_gray'
            self.clipping = 'magenta'

        else:
            assert scheme == 'none' or scheme is None
            self.use_color = False
            self.column = ''
            self.error = [''] * 3
            self.logo = [''] * 6
            self.low_bq = [''] * 2
            self.low_mapq = ''
            self.supplementary = ''
            self.out_of_region = ''
            self.clipping = ''

    def color_logo(self, nt):
        nt_upper = nt.text.upper()
        if nt_upper == 'A':
            nt.fg(self.logo[2])
        elif nt_upper == 'C':
            nt.fg(self.logo[3])
        elif nt_upper == 'G':
            nt.fg(self.logo[4])
        elif nt_upper == 'T':
            nt.fg(self.logo[5])
        return nt

    def color_insertion_sign(self, text):
        return text.fg(self.logo[1] if self.use_logo else self.error[1])

    def color_insertion_seq(self, seq, ref_nt, qual, min_bq):
        res = str(self.color_base(self.text(seq[0]), ref_nt, qual, min_bq))
        if self.use_logo:
            res += '+'
            for nt in seq[1:]:
                res += str(self.color_base(self.text(nt), 'X', None, None))
        else:
            res += str(self.color_insertion_sign(self.text('+')))
            res += seq[1:]
        return res

    def color_deletion(self, text):
        return text.fg(self.logo[0] if self.use_logo else self.error[0])

    def color_base(self, query_nt, ref_nt, qual, min_bq):
        if qual is not None and qual < min_bq:
            query_nt.text = '?'
            return query_nt.fg(self.low_bq[1])

        if query_nt.text.upper() == ref_nt.upper():
            query_nt.text = '.' if query_nt.text.isupper() else ','
            return query_nt

        if self.use_logo:
            self.color_logo(query_nt)
        elif ref_nt.upper() != 'N':
            query_nt.fg(self.error[2])
        return query_nt

    def text(self, text):
        return Text(text, self.use_color)


class Text:
    def __init__(self, text, use_color):
        self.text = text
        self.attrs = [] if use_color else None

    def fg(self, color):
        if self.attrs is not None:
            self.attrs.append(colored.fg(color))
        return self

    def fg_if(self, cond, color):
        if cond and self.attrs is not None:
            self.attrs.append(colored.fg(color))
        return self

    def bg(self, color):
        if self.attrs is not None:
            self.attrs.append(colored.bg(color))
        return self

    def bg_if(self, cond, color):
        if cond and self.attrs is not None:
            self.attrs.append(colored.bg(color))
        return self

    def attr(self, attr):
        if self.attrs is not None:
            self.attrs.append(colored.attr(attr))
        return self

    def attr_if(self, cond, attr):
        if cond and self.attrs is not None:
            self.attrs.append(colored.attr(attr))
        return self

    def __str__(self):
        return colored.stylize(self.text, self.attrs) if self.attrs else str(self.text)
