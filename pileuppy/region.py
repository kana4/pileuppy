import re
import math

from pileuppy._version import __version__

__author__ = 'Timofey Prodanov, Vikas Bansal'
__credits__ = __author__.split(', ')
__license__ = 'MIT'
__maintainer__ = __credits__[0]


def adjust_window_size(length, window):
    n_windows = math.ceil(length / window)
    return int(math.ceil(length / n_windows))


class Region:
    def __init__(self, chrom, start, end):
        if start > end:
            raise ValueError('Cannot construct pileup: region start > end\n')
        self.chrom = chrom
        self.start = start
        self.end = end

    @classmethod
    def from_str(cls, region):
        region1 = region.replace(',', '')
        m = re.match(r'([^:]+):(\d+)([-^])(\d+)$', region1)
        if m is None:
            m = re.match(r'([^:]+):(\d+)$', region1)
            if m is None:
                raise ValueError('Failed to parse region "%s"\n' % region)
            y = 0
            radial = True
        else:
            y = int(m.group(4))
            radial = m.group(3) == '^'

        chrom = m.group(1)
        x = int(m.group(2)) - 1
        if radial:
            start = max(0, x - y)
            end = x + y + 1
        else:
            start = x
            end = y

        return Region(chrom, start, end)

    def __len__(self):
        return self.end - self.start

    def __str__(self):
        return '%s:%d-%d' % (self.chrom, self.start + 1, self.end)

    def add_padding(self, size, reference):
        size = max(0, size)
        region = Region.__new__(Region)
        region.chrom = self.chrom
        region.start = max(0, self.start - size)
        region.end = self.end + size
        if reference is not None:
            try:
                region.end = min(region.end, reference.get_reference_length(self.chrom))
            except KeyError as e:
                raise KeyError('Failed to construct pileup: chromosome %s not in the reference\n' % e)
        return region

    def contains(self, pos):
        return self.start <= pos < self.end

    def split_into_smaller(self, max_size):
        size = adjust_window_size(len(self), max_size)
        for start in range(self.start, self.end, size):
            yield Region(self.chrom, start, start + size)
