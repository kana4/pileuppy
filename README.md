[![Install with bioconda](https://img.shields.io/conda/v/bioconda/pileuppy.svg?label=Install%20with%20conda&color=blueviolet&style=flat-square)](https://anaconda.org/bioconda/pileuppy)
[![Updated](https://anaconda.org/bioconda/pileuppy/badges/latest_release_date.svg?style=flat-square)](https://anaconda.org/bioconda/pileuppy)

Pileuppy
========

Colorful and fast tool designed to draw alignment pileup: each row in the output corresponds to a genomic position
and each column represents a read.

Created by Timofey Prodanov `timofey.prodanov[at]gmail.com` and Vikas Bansal at the University of California San Diego.

Usage
=====

Pileuppy can manage multiple BAM files and even separate different samples stored in the same BAM file.
`pileuppy` has similar command line arguments as `samtools mpileup`.

```bash
pileuppy in1.bam [in2.bam ...] -f genome.fa -r chr1:10,000-10,010
```

You can pipe output to `less`:
```bash
pileuppy in1.bam [in2.bam ...] -f genome.fa -r chr1:10,000-10,010 | less -SR
```
or save to file with `-o pileup.txt`.

Additionally, you can change color scheme with `-S white`, `-S ansi`. Use `-S none` to disable color output.
Sometimes there are black columns across the pileup. They can be removed by using different color scheme, or by using
`-C/--no-columns`.

Sometimes we want to examine a small SNV and see read alignments close to it. Pileuppy allows two convinient ways to
do that:
- `-r chr:pos^10` will produce the same pileup as `-r chr:(pos - 10)-(pos + 10)`
- `-r chr:pos -d 10` will show pileup for the region `chr:(pos - 10)-(pos + 10)`, but will not show
reads that do not cover `chr:pos`.

Installation
============

You can install `pileuppy` using `conda`:
```bash
conda install -c bioconda pileuppy
```

Otherwise, you can clone this repository and run `setup.py`
(requires installed `argparse`, `pysam` and optional `colored`):
```bash
git clone https://gitlab.com/tprodanov/pileuppy.git
python3 setup.py install
```

Long regions
============

To make output clearer, `pileuppy` shows each read as a separate column. Because of that, it is impossible to
construct pileup for the whole genome. Additionally, by default `pileuppy` splits long regions into 150 bp regions
(can be changed with `--size INT`).

Comparison with `samtools mpileup`
==================================

We used `samtools mpileup` and `pileuppy` to construct pileups of the same region
in the same BAM file containing PacBio long reads:

![samtools mpileup](https://i.imgur.com/C5Cs862.png)
![pileuppy](https://i.imgur.com/ydp6mFq.png)

Not only `pileuppy` output is more colorful and easier to read, it was constructed over 10 times faster.

See also
========

If you work with long reads (PacBio / Oxford Nanopore), you may be interested in these tools:
- [DuploMap](https://gitlab.com/tprodanov/duplomap): tool designed to increase precision and recall
    of long-read alignments in segmental duplications,
- [Longshot](https://github.com/pjedge/longshot/): fast and accurate long-read variant calling tool.
